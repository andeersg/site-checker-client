<?php

namespace Drupal\site_checker_client\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Returns JSON or 404.
 */
class SiteCheckerClientController extends ControllerBase {

  /**
   * Builds the state variable overview page.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   JSON page response.
   */
  public function systemState() {

    $token = \Drupal::config('site_checker_client.settings')->get('token');
    if (!$token) {
      throw new NotFoundHttpException();
    }

    $valid_token = $token === $_GET['token'];
    if (!$valid_token) {
      throw new NotFoundHttpException();
    }

    $theme_name = \Drupal::service('theme.manager')->getActiveTheme()->getName();

    $php_version_array = explode('.', phpversion());
    $php_version = $php_version_array[0] . '.' . $php_version_array[1];

    $modules = \Drupal::service('extension.list.module')->getAllInstalledInfo();
    $modules = array_map('\Drupal\site_checker_client\Controller\SiteCheckerClientController::prepareModuleData', $modules);

    unset($modules['minimal']);

    $response = [
      'core_version' => \Drupal::VERSION,
      'php_version' => $php_version,
      'default_theme' => $theme_name,
      'theme' => $this->prepareThemeData(\Drupal::service('extension.list.theme')->getExtensionInfo($theme_name)),
      'modules' => $modules,
      'server' => explode('/', $_SERVER['SERVER_SOFTWARE'])[0],
    ];

    return new JsonResponse($response);
  }

  /**
   * Remove fields not necessary.
   *
   * @param array $theme
   *   The info about the theme.
   * @param string $key
   *   The machine name of the theme.
   *
   * @return array
   *   The formatted array.
   */
  public function prepareThemeData($theme, $key = '') {
    unset($theme['regions']);
    unset($theme['regions_hidden']);
    unset($theme['libraries']);
    unset($theme['libraries-override']);
    unset($theme['libraries_extend']);
    unset($theme['libraries_override']);
    unset($theme['conditional-stylesheets']);
    unset($theme['features']);

    // $theme['key'] = $key;

    return $theme;
  }

  public function prepareModuleData($module) {
    unset($module['type']);
    unset($module['php']);
    unset($module['core_version_requirement']);
    unset($module['dependencies']);
    unset($module['configure']);
    unset($module['mtime']);
    unset($module['tags']);
    unset($module['lifecycle']);
    unset($module['test_dependencies']);
    unset($module['install']);

    return $module;
  }

}

