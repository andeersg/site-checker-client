<?php

namespace Drupal\site_checker_client\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class ModuleConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'site_checker_client_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'site_checker_client.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('site_checker_client.settings');
    $form['token'] = [
      '#title' => $this->t('Token'),
      '#default_value' => $config->get('token'),
      '#type' => 'textfield',
      '#description' => 'This token is required to fetch information.',
    ];
    $form['path'] = [
      '#markup' => Url::fromRoute('site_checker_client.system_state', ['token' => $config->get('token')], ['absolute' => TRUE])->toString(),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('site_checker_client.settings')
      ->set('token', $values['token'])
      ->save();
  }

}

